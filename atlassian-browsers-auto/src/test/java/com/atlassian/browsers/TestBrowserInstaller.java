package com.atlassian.browsers;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Units tests for {@link BrowserInstaller}.
 *
 * @since v1.1
 */
public class TestBrowserInstaller
{
    @Rule public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testUnknownBrowserIsNull()
    {
        BrowserInstaller unknown = BrowserInstaller.typeOf("unknown");
        assertNull(unknown);
    }

    @Test
    public void testBrowserInstallerWorksAsExpected()
    {
        BrowserInstaller firefox = BrowserInstaller.typeOf("firefox");
        assertEquals(BrowserType.FIREFOX, firefox.getBrowser());
        assertEquals(OS.getType(), firefox.getOS());
    }

    @Test
    public void testVersionedBrowserWorksAsExpected()
    {
        BrowserInstaller firefox = BrowserInstaller.typeOf("firefox-3.5");
        assertEquals(BrowserType.FIREFOX, firefox.getBrowser());
        assertEquals(OS.getType(), firefox.getOS());
    }

    @Test
    public void testSpecificPathForBrowserWorksAsExpected()
    {
        BrowserInstaller firefox = BrowserInstaller.typeOf("firefox:path=/a/b/c");
        assertNull(firefox);
    }

    @Test
    public void windowsIeShouldInstallWithoutErrors() throws IOException
    {
        final InstallConfigurator mockConfigurator = FakeInstallConfigurator.spy();
        final BrowserInstaller ieWindows = BrowserInstaller.IE_WINDOWS;
        ieWindows.install(temporaryFolder.newFolder(), new BuiltInConfigurator(mockConfigurator));
        verify(mockConfigurator).setupBrowser(Mockito.argThat(BrowserConfigMatchers.withBrowserType(BrowserType.IE)));
        verifyNoMoreInteractions(mockConfigurator);
    }
}
