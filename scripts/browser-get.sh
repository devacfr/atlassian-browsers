#!/bin/bash

set -euo pipefail

# browser = firefox, chrome
# os = linux, linux64, osx
source "$(dirname "$0")/helpers.sh"

unpackTarBz2 () {
    local name=$1
    log "Unpacking ${name}.tar.bz2"
    bunzip2 "${name}.tar.bz2"
    tar -xf "${name}.tar"
    rm ${name}.tar
}

unpack7zip () {
    local name=$1
    local exename
    if [ -z "${2:-}" ]; then
        exename="${name}.exe"
    else
        exename="$2"
    fi
    local szname="${name}.7z"

    if cmd_exists 7z; then
        local szcommand="7z"
    elif cmd_exists p7zip; then
        local szcommand="p7zip"
    fi

    if [ -z "$szcommand" ]; then
        fail "Unable to extract files from ${name}.exe\nInstall p7zip.\neg. brew install p7zip"
    fi

    mkdir $name
    qpushd $name
    if [ $szcommand = "7z" ]; then
        log "Unpacking $exename"
        errors="$($szcommand x -y "$exename" 2>&1)" || fail "Failed to extract 7zip binary: $exename: $errors"
    else
        # on Linux p7zip requires .7z suffix ...
        mv "$exename" "$szname"
        log "Unpacking $szname"
        errors="$($szcommand -d "$szname" 2>&1)" || fail "Failed to extract 7zip binary: $szname: $errors"
    fi

    qpopd
    rm -f "$exename"
    rm -f "$szname"
}

mountDmg () {
    local name=$1

    mount_point=${name}-mount
    mkdir -p ${mount_point}

    if cmd_exists hdiutil
    then
        log "Mounting ${name}.dmg"
        hdiutil attach "${name}.dmg" -mountpoint "${mount_point}" -quiet
        return "$?"
    elif cmd_exists 7z; then
        log "Extracting .dmg"
        errors="$(7z x -y "${name}.dmg" -o"${mount_point}" 2>&1)" || fail "Failed to extract dmg: $errors"
        # DMG files when extracted with 7z have an additional folder prefix
        mv "${mount_point}/"*/* "${mount_point}"
    else
        log "Converting .dmg to .img for mounting"
        if cmd_exists dmg2img
        then
            dmg2img -s "${name}.dmg" "${name}.img"
            sudo modprobe hfsplus
            sudo mount -t hfsplus -o loop "${name}.img" "${mount_point}"
            return "$?"
        else
            rm -rf "${mount_point}"
            fail "dmg2img not found, please install it\n  eg. sudo apt-get install dmg2img"
        fi
    fi
}

unmountDmg () {
    local name=$1

    mount_point=${name}-mount

    log "Unmounting ${mount_point}"

    if cmd_exists hdiutil
    then
        hdiutil detach "${mount_point}" -quiet
    elif ! cmd_exists 7z; then
        sudo umount "${mount_point}"
        rm "${name}.img"
    fi

    rm "${name}.dmg"
    rm -rf "${mount_point}"
}

untarLzma () {
    local file=$1
    tar --lzma -xf ${file} &> /dev/null
}

unpackDebPackage () {
    local name=$1

    ar x "${name}.deb" data.tar.xz
    mkdir -p /tmp/tmpdeb
    mv data.tar.xz /tmp/tmpdeb/.
    qpushd /tmp/tmpdeb

    log "Attempting to unpack deb package."
    if untarLzma /tmp/tmpdeb/data.tar.xz
    then
        log "Deb package unpacked."
    else
        if cmd_exists 7z
        then
            7z -y x /tmp/tmpdeb/data.tar.xz &> /dev/null
            tar -xf data.tar
            log "Deb package unpacked with 7z."
        else
            fail "Unable to extract the data.tar.xz file from ${name}.deb\nInstall p7zip.\neg. brew install p7zip"
        fi
    fi

    qpopd

}

packageFirefoxForOS () {
    local os=$1
    local name=$2
    local version=$3

    case "$os" in
        linux | linux64)
            unpackTarBz2 $name
            qpushd firefox
            echo "Firefox for $os: version - ${version}" > browser.package
            zipFile $name "*"
            qpopd
            rm -rf firefox
            ;;
        osx)
            mountDmg $name || fail "Unable to mount ${name}.dmg, the downloaded image might be invalid"
            qpushd ${name}-mount/Firefox.app/
            mkdir ${name}
            cp -R * ${name}
            qpopd
            qpushd ${name}
            echo "Firefox for $os: version - ${version}" > browser.package
            zipFile $name "*"
            qpopd
            unmountDmg $name
            rm -rf ${name}
            ;;
        windows)
            unpack7zip $name
            qpushd $name/core
            echo "Firefox for $os: version - ${version}" > browser.package
            zipFile $name "*"
            qpopd
            rm -rf $name
            ;;
        *)
            fail "Unknown Operating System: ${1}"
            ;;
    esac
}

packageChromeForOS () {
	local name=$2
	local version=$3
	case "$1" in
		linux | linux64)
			qpushd /tmp
            local currentVersion="$(ar -p ${name}.deb control.tar.gz | tar xO control | grep "Version: ")"
            currentVersion="${currentVersion#"Version: "}"
            if [[ "$currentVersion" != "$version"* ]]; then
                fail "Wrong Google Chrome version: $version. Latest version is $currentVersion"
            fi
			unpackDebPackage ${name}
			rm ${name}.deb
			qpushd /tmp/tmpdeb/opt/google/chrome
			echo "Chrome for $1: version - ${version}" > browser.package
			zipFile $name "*"
			qpopd
			qpopd
			rm -rf /tmp/tmpdeb
			;;
		osx)
			mountDmg ${name}
			sleep 5
			pushd "${name}-mount/"
			if ! find . -type d -name "${version}" >/dev/null; then
			    local currentVersion="$(ls "$(find . -type d -name Versions | head -n1)" | head -n1)"
			    qpopd
			    unmountDmg ${name}
			    fail "Wrong Google Chrome version: $version. Latest version is $currentVersion"
			fi
			mkdir ${name}
			cp -R "Google Chrome.app" "${name}"
			qpopd
			qpushd ${name}
			echo "Chrome for osx: version - ${version}" > browser.package
			zipFile "${name}" "*"
			qpopd
			unmountDmg ${name}
			rm -rf ${name}
			;;
		windows)
		    unpack7zip $name
		    qpushd $name
		    unpack7zip chrome ../chrome.7z
		    if [ ! -d "chrome/Chrome-bin/${version}" ]; then
		        local currentVersion="$(ls "chrome/Chrome-bin/" | head -n1)"
			    qpopd
			    fail "Wrong Google Chrome version: $version. Latest version is $currentVersion"
		    fi
            qpushd chrome/Chrome-bin
            echo "Chrome for windows: version - ${version}" > browser.package
            zipFile "${name}" "*"
            qpopd
            qpopd
            rm -rf $name
		    ;;
		*)
			fail "Unknown Operating System: ${1}"
			;;
	esac

}

getFirefoxForOS () {
    local os=$1
    local version=$2
    local name=/tmp/firefox-${version}-$os

    case "$os" in
        linux)
            file=${name}.tar.bz2
            url="https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${version}/linux-i686/en-US/firefox-${version}.tar.bz2"
            ;;
        linux64)
            file=${name}.tar.bz2
            url="https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${version}/linux-x86_64/en-US/firefox-${version}.tar.bz2"
            ;;
        osx)
            file=${name}.dmg
            url="https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${version}/mac/en-US/Firefox%20${version}.dmg"
            ;;
        windows)
            file=${name}.exe
            url="https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${version}/win32/en-US/Firefox%20Setup%20${version}.exe"
            ;;
        *)
            fail "Invalid Operating system ${os}"
            ;;
    esac

    getFile "${file}" "${url}" || fail "Failed to retrieve firefox from ${url} and/or store in ${file}"
    packageFirefoxForOS $os $name ${version}
    log "Browser has been packaged: ${name}.zip"

}

getChromeForOS () {
	local version=$2
	name=/tmp/chrome-${version}-$1
	case "$1" in
		linux64)
			file=${name}.deb
			url="https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
			;;
		osx)
			file=${name}.dmg
			url="https://dl.google.com/chrome/mac/stable/CHFA/googlechrome.dmg"
			;;
		windows)
		    file=${name}.exe
            updatexml="$(curl -XPOST http://tools.google.com/service/update2 --data '<?xml version="1.0" encoding="UTF-8"?><request protocol="3.0" version="1.3.32.7" shell_version="1.3.32.7" ismachine="1" installsource="update3web-ondemand" dedup="cr">
<hw physmemory="4" sse="1" sse2="1" sse3="1" ssse3="1" sse41="0" sse42="0" avx="0"/>
<os platform="win" version="10.0.14393.693" sp="" arch="x64"/>
<app appid="{8A69D345-D564-463C-AFF1-A69D9E530F96}" version="" nextversion="" ap="x64-stable-multi-chrome"><updatecheck/></app></request>')"
            url="$(python3 <(cat <<PYTHON
import sys
from xml.etree.ElementTree import fromstring
root = fromstring(sys.stdin.read())
package = root.find('app/updatecheck/manifest/packages/package').attrib['name']
print([i.attrib['codebase'] + package for i in root.findall('app/updatecheck/urls/url')][-1])
PYTHON
) <<<"$updatexml")"
            ;;
		*)
			fail "Invalid Operating system ${1}"
			;;

    esac

    getFile "${file}" "${url}"
    packageChromeForOS $1 $name ${version}
    log "Browser has been packaged: ${name}.zip"
}

getBrowserForOs () {
  qpushd /tmp
  local os=$2
  local version=$3
  case "$1" in
    firefox)
        getFirefoxForOS $os $version
        ;;
    chrome)
        getChromeForOS $os $version
        ;;
    *)
        fail "Invalid Browser ${1}"
        ;;
  esac
}

if isHelpRequested "$*"
then
    log "$0 <browser> <os> <version>"
    log "  browser: chrome,firefox"
    log "  os: linux,osx,windows"
    log "  version: any version number of the browser to get. $CHROME_VERSION_WARNING"
    exit
fi

browser=$1
os=$2
version=$3

getBrowserForOs $browser $os $version
